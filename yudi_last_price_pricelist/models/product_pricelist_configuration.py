from odoo import _, api, models, fields

class ProductPricelistConfiguration(models.Model):
    _inherit="product.pricelist.configuration"

    last_price = fields.Float(string='Harga Beli Terakhir', compute='_compute_last_price', store=True)
    last_date = fields.Date(string='Tanggal Beli Terakhir', compute='_compute_last_price', store=True)

    def _compute_last_price(self):
        ''' Function to Compute last price and ;ast date purchasing '''
        for this in self:
            none = False
            if this.product_id:
                template = this.product_id.product_tmpl_id
                if template:
                    this.last_price = template.last_price
                    this.last_date = template.last_date
                    none = True
            if not none:
                this.last_price = 0
                this.last_date = False


