from odoo import _, api, models, fields

class ProductTemplate(models.Model):
    _inherit="product.template"
    
    @api.depends('seller_ids')
    def _compute_last_price_date(self):
        ''' Function to Compute last price and ;ast date purchasing on product form '''
        for this in self:
            if this.seller_ids:
                this.last_price_product = this.seller_ids[-1].price
                this.last_date_product = this.seller_ids[-1].date_create
            else:
                this.last_price_product = this.standard_price
                this.last_date_product = this.create_date
                
    last_price_product = fields.Float(
        string='Harga Beli Terakhir', compute='_compute_last_price_date')
    last_date_product = fields.Date(
        string='Tanggal Beli Terakhir', compute='_compute_last_price_date')
    
