# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Yudi - Last Price Product',
    'version': '13.0.0.0.1',
    'author': 'Port Cities',
    'website': 'https://www.portcities.net',
    'category': 'Sales, Purchase',
    'author': 'Portcities Ltd',
    'website': 'http://www.portcities.net',
    'license': 'LGPL-3',
    'summary': 'This module for adding information for Last Purchase of Product',
    'description': """
        v 0.0.1 \n
            By hajiyanto \n
            1) Adding Information Last Price and Last Date Purchasing of Product\n
    """,
    'depends': [
        'product', 
        'yudi_customize_so', 
        'yudi_sale_configuration',
    ],
    'data': [
        'views/product_template_views.xml',
        'views/product_pricelist_configuration.xml',
    ],
    'installable': True,
}
