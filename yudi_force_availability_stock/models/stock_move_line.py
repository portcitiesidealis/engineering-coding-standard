from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class ResPartner(models.Model):
    _inherit = 'stock.move.line'

    is_force_availability = fields.Boolean(default=False)
    sale_id = fields.Many2one('sale.order', string="Sale Order")