from odoo import models, fields, api, _
from odoo.exceptions import UserError

class StockPicking(models.Model):
    _inherit = 'stock.picking'


    def button_validate(self):
        ''' Inherti butotn validate DO, 
        the purpose to adding information history of force avalialbiulty '''
        res = super(StockPicking, self).button_validate()
        if self.move_line_ids_without_package:
            move_line = self.move_line_ids_without_package.filtered(lambda move: move.is_force_availability == True)
            for line in move_line:
                new_history_force_avalaibility = self.env['force.availability'].sudo().create({
                    'product_id': line.product_id.id,
                    'location_id': line.location_id.id,
                    'quantity': line.qty_done,
                    'picking_id': line.picking_id.id,
                    'sale_id': self.sale_id.id,
                    'date': fields.datetime.now(),
                })
        return res
