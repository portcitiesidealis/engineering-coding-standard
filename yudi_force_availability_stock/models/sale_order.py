from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.tools.translate import _


class SaleOrder(models.Model):
    _inherit = 'sale.order'


    def action_confirm(self):
        ''' Inherit action confirm to fill operation 
        details(stock move) even stock of product minus/not availability '''
        res = super(SaleOrder, self).action_confirm()
        picking_ids = self.picking_ids.filtered(lambda pick: pick.location_dest_id.usage == 'transit')
        for pick in picking_ids:
            for move_line in pick.move_ids_without_package:
                if not move_line.move_line_ids:
                    pick.move_line_ids_without_package =  [(0,0,{
                        'date': self.date_order,
                        'reference': pick.name,
                        'origin': self.name,
                        'product_id': move_line.product_id.id,
                        'location_id': move_line.location_id.id,
                        'location_dest_id': move_line.location_dest_id.id,
                        'company_id': pick.company_id.id,
                        'product_uom_id': move_line.product_uom.id,
                        'qty_done': move_line.product_uom_qty,
                        'is_force_availability': True,
                        'move_id': move_line.id
                    })]
            for detail_operation in pick.move_line_ids_without_package.filtered(lambda line: line.is_force_availability == False):
                detail_operation.qty_done = detail_operation.move_id.product_uom_qty
        return res
