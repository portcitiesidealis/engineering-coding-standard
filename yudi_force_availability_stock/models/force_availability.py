from odoo import models, fields, api, _
from odoo.exceptions import UserError


class ForceAvailability(models.Model):
    _name = "force.availability"
    _inherit = ['mail.thread']
    _description = "Force Availability"
    _order = 'create_date desc, id desc'

    product_id = fields.Many2one('product.product', string="Nama Produk")
    location_id = fields.Many2one('stock.location', string="Lokasi")
    quantity = fields.Float()
    picking_id = fields.Many2one('stock.picking', string="Picking")
    sale_id = fields.Many2one('sale.order', string="Sale Order")
    date= fields.Datetime(string='Tanggal')