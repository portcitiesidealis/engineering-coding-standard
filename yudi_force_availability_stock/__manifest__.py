# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Yudi - Force Availability Stock",
    'version': '13.0.0.1.0',
    'summary': "Customize Force Availability Stock",
    'website': "http://www.portcities.net",
    'author': "Portcities Ltd",
    'summary': "Customize Force Availability Stock",
    'category': 'inventory',
    'description': '''
            V.1.0 By Hajiyanto Prakoso \n
                     1) this feature to force availability stock when stock is minus
                     2) when confirm SO will be auto filled Operations details on DO
                   ''',
    'depends': ['stock', 'sale_management'],
    'data': [
        'security/force_avalaibilty_security.xml',
        'security/ir.model.access.csv',
        'views/force_avalaibilty_report.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
