# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Yudi - Vehicle Picking',
    'version': '13.0.0.1.0',
    'author': 'Port Cities',
    'website': 'https://www.portcities.net',
    'license': 'LGPL-3',
    'summary': 'Add Information driver and Car in stock picking batch',
    'description': """
        v 0.0.1 \n
            By hajiyanto \n
            1) Add Information driver and Car in stock picking batch
            2) The purpose to know availabe driver and car to pick batch transfer
         v 0.0.2 \n
            By hajiyanto \n
            1) Add button to create invoice and print picking batch on batch line
            2) Add restriction when create invoice and print on batch transfer
            3) Adding Information Picking/DO per customer that was not yet created into Batch Transfer
    """,
    'depends': [
        'sale',
        'yudi_report_term_delivery'
        
    ],
    'data': [
        'security/batch_transfer_groups.xml',
        'security/ir.model.access.csv',
        'views/driver.xml',
        'views/mobil.xml',
        'views/stock_picking_batch.xml',
        'views/picking.xml',
        'views/batch_picking_view.xml'
    ],
    'installable': True,
}
