from odoo import api, fields, models, _
from odoo.exceptions import UserError
import base64
from odoo.tools.translate import _
import json,pytz
import datetime

class StockPickingBatch(models.Model):
    _inherit = 'stock.picking.batch'

    driver = fields.Many2one('driver')
    no_plat = fields.Many2one('mobil',string='Plat Nomor')
    truck_keluar = fields.Datetime()
    truck_masuk = fields.Datetime()
    user_id = fields.Many2one(
        'res.users', string='Responsible', tracking=True, check_company=True, default=lambda self: self.env.user,
        readonly=True ,help='Person responsible for this batch transfer')
    usergroup = fields.Boolean(default = lambda self: self.env.user.has_group("yudi_customize_so.group_superadmin"))
    is_truck_back = fields.Boolean(default= False)
    state = fields.Selection(selection_add=([('delivered', 'Delivered'), ('done',)]))
    html = fields.Html()
    tot_check_do = fields.Integer()

    @api.onchange('driver')
    def onchange_no_plat(self):
        self.no_plat = self.driver.no_plat
        self._restrict_batch_transfer()
        
    @api.onchange('picking_ids')
    def onchange_picking_ids(self):
        if any(picking.picking_type_id.code == 'internal' and picking.picking_type_id.sequence_code == 'PICK' for picking in self.picking_ids):
            raise UserError(_("Can't Add Picking Document, please change Add Delivery Order Document"))
        tot_batch_line = len(self.picking_ids)
        self.tot_check_do = self.tot_check_do - 1
        if self.tot_check_do < tot_batch_line:
            msg = self.check_order_blm_picking()
            check = {}
            if msg:
                check['warning'] = {
                    'title': _('Reminder!'),
                    'message': _(self.html)
                                    }
            return check
            
            
    def check_order_blm_picking(self):
        tot_batch_line = len(self.picking_ids)
        current_batch_transfer = self.picking_ids[-1]
        delivery_not_batch = self.env['stock.picking'].search([('batch_id', '=', False), ('partner_id', '=', current_batch_transfer.partner_id.id),
                                                               ('picking_type_id.code', '=', 'outgoing'), (
                                                                   'picking_type_id.sequence_code', '=', 'OUT'),
                                                               ('state', 'in', ['waiting', 'assigned', 'confirmed'])])
        msg = ""
        if delivery_not_batch:
            tot_do_not_batch = len(delivery_not_batch)
            dt_do = self.get_data_sale_order_picking(delivery_not_batch)
            self.tot_check_do = tot_batch_line
            change_data = self.change_format_is_table(dt_do)
            name = current_batch_transfer.partner_id.name
            msg = u'<p>Data Customer dengan nama %s masih ada order yang belum masuk ke Batch Transfer :<br>'  \
                                'Total SO yang belum memiliki Batch = %s <br>' \
                                'Informasi DO = <br>' \
                                '%s'\
                                '</p>' % (
                                    name, tot_do_not_batch, change_data,)
            self.html = msg
        return msg

    def get_data_sale_order_picking(self, order_ids):
        # make table detail sale order
        # return string table format html
        # header
        header1 = 'Reference'
        header2 = 'Schedule Date'
        header3 = 'Customer'
        header4 = 'Source Document'
        header5 = 'Delivery Address'
        header6 = 'Total'
        table = '<table>'\
                '<tr><td>%s</td><td>%s</td><td>%s</td>'\
                '<td>%s</td><td>%s</td><td>%s</td></tr>' % (header1, header2, header3, header4,
                                                            header5, header6)
        for this in order_ids:
            # detail table
            tgl = this.scheduled_date.strftime("%m/%d/%Y, %H:%M:%S")
            partner = this.partner_id.name
            sale_order = this.sale_id.name
            total = this.sale_id.amount_total
            txt = "<tr><td>%s</td><td>%s</td><td>%s</td>"\
                "<td>%s</td><td>%s</td><td>%s</td></tr>" % (this.name, tgl, partner, sale_order, this.deliv_address,
                                                            '{:,.2f}'.format(total))
            table = table+txt
        table = table + "</table>"
        return table

    def change_format_is_table(self, message):
        # function for change format and adding style on table
        # based on idea this function cause format text can be contain style HTML ;
        txt = message
        if txt.find('<table') >= 0:
            txt = txt.replace('<table', '<table style="width:100%;"')
            txt = txt.replace(
                '<td', '<td style="border: 1px solid black; text-align: center;"')
        return txt

    def confirm_picking_batch(self):
        self._check_company()
        if any(picking.picking_type_id.code == 'internal' and picking.picking_type_id.sequence_code == 'PICK' for picking in self.picking_ids):
            raise UserError(_("Can't Add Picking Document, please change Add Delivery Order Document"))
        self.truck_keluar = datetime.datetime.now()
        for picking in self.picking_ids:
            picking_waiting = picking.filtered(lambda p: p.state in ['confirmed', 'waiting', 'assigned'])
            picking.driver = self.driver.id
            picking.no_plat = self.no_plat.id
            if picking.picking_type_id.code == 'outgoing'and not picking.invoice_ids:
                raise UserError(_("Can't Confirm Batch Picking %s cause has not invoice, please create invoise first!!" % (picking.name)))
            if picking_waiting:
                self.confirm_picking()
        self.write({'state': 'in_progress'})


    def print_surat_pickings(self):
        if self.picking_ids.filtered('printed') and not self.env.user.has_group('yudi_report_term_delivery.group_stock_picking_batch_reprint'):
            raise UserError(_("Some Pickings has already been printed. You don't have the access to reprint Batch Pickings"))
        for picking in self.picking_ids:
            picking.sale_id.print_counted_invoice = picking.sale_id.print_counted_invoice + 1
        res = self.env.ref('yudi_report_term_delivery.action_report_surat_jalan').report_action(self.picking_ids)
        self.env.user.create_print_history(self.picking_ids.mapped('name'))
        self.picking_ids.write({'printed': True, 'printed_by': self.env.uid})
        return res


    def action_state_to_draft(self):
        self.write({'state': 'draft'})

    def action_cancel_batch(self):
        return self.write({'state': 'cancel'})

    def action_done_batch(self):
        if self.state == 'delivered' and all(picking.state in ('assigned','done') for picking in self.picking_ids):
            ready_pickings = self.picking_ids.filtered(lambda pick: pick.state == 'assigned')
            if ready_pickings:
                self.env['stock.immediate.transfer'].create({'pick_ids': [(6,0, ready_pickings.ids)]}).process()
        else:
            raise UserError(_('Masih Ada Picking Dengan Status Selain Ready dan Done!'))
        if self.state == 'delivered' and not all(picking.state == 'done' for picking in self.picking_ids):
            raise UserError(_('Please Validate All Delivery Order'))
        return self.write({'state': 'done'})

    def action_truck_back(self):
        self.truck_masuk = datetime.datetime.now()
        self.write({'is_truck_back': True, 'state': 'delivered'})
        
    def recompute_batch_state(self):
        self.write({'state': 'delivered'})

    def _restrict_batch_transfer(self):
        if not self.driver:
            return
        m_data = self.search(
            [('driver', '=', self.driver.id), ('state', '=', 'draft')])
        for this in m_data:
            if this.driver.id == self.driver.id and this.state == 'draft':
                raise UserError(_("Driver already selected in record with draft state"))

    @api.model
    def create(self, vals):
        res = super(StockPickingBatch, self).create(vals)
        if vals.get('driver'):
            self._restrict_batch_transfer()
        return res

    def write(self, vals):
        if vals.get('driver'):
            m_data = self.search(
                [('driver', '=', vals.get('driver')), ('state', '=', 'draft')])
            if m_data:
                raise UserError(
                    _("Driver already selected in record with draft state"))
        return super(StockPickingBatch, self).write(vals)
