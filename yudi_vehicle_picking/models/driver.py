from odoo import api, fields, models, _
from odoo.exceptions import UserError

class Driver(models.Model):
    _name = 'driver'
    _description = 'Driver'

    name = fields.Char()
    address = fields.Text()
    no_plat = fields.Many2one('mobil')
    vehicle_brand = fields.Char(related="no_plat.vehicle_name")
    vehicle_color = fields.Char(related="no_plat.color")
    job_position = fields.Char(default='DRIVER')
    no_ktp_driver = fields.Char()
    mobile = fields.Char()
    email = fields.Char()
    image_1920 = fields.Image()

    @api.constrains('name')
    def _validat_name(self):
        m_data = self.search([])
        for rec in m_data:
            if rec.name.lower() == self.name.lower() and rec.id != self.id:
                raise UserError(_("Driver already exist"))
                
