from odoo import api, fields, models, _
from odoo.exceptions import UserError


class StockPicking(models.Model):
    ''' Inherit Model Stock Picking '''
    _inherit = 'stock.picking'

    driver = fields.Many2one('driver')
    no_plat = fields.Many2one('mobil')
    

    def display_source_document(self):
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': self.sale_id._name,
            'res_id': self.sale_id.id,
            'views': [(False,'form')],
            'type': 'ir.actions.act_window',
            'target': 'current',
        }

    invoice_ids = fields.Many2many(related="sale_id.invoice_ids")
    amount_due = fields.Float(string="Amount Due", compute="_compute_data_sale_id")
    invoice_count = fields.Integer(related="sale_id.invoice_count")

    def _compute_data_sale_id(self):
        for this in self:
            amount_due_picking = 0
            amount_due_inv = 0
            amount_due_cn = 0
            if this.sale_id and this.invoice_count > 0:
                for inv in this.invoice_ids.filtered(lambda i: i.type == 'out_invoice'):
                    amount_due_inv += inv.amount_residual
                for cn in this.invoice_ids.filtered(lambda i: i.type == 'out_refund'):
                    amount_due_cn += cn.amount_residual
                amount_due_picking = amount_due_inv - amount_due_cn
            this.amount_due = amount_due_picking

    def action_create_and_print_invoice(self):
        # create invoice and print invoice
        so = self.sale_id
        so.print_counted_invoice = so.print_counted_invoice + 1
        so.write({
			'printed_invoice' : True,
			'date_invoice' : fields.datetime.now()
		})
        for picking in so.picking_ids.filtered(lambda x: x.picking_type_id.code == 'internal'):
            if picking.state != 'done':
                raise UserError(
                    _("Can't Create Invoice, Because Picking %s has not validate" % picking.name))
        inv = so._create_invoices(final=True)
        inv.action_post()
        res = self.env.ref('yudi_report_term_delivery.action_report_invoice_dan_barcode_do')
        return res.report_action(inv)
