from odoo import api, fields, models, _
from odoo.exceptions import UserError
import base64

class ReportInvoiceWithPayment(models.AbstractModel):
    _name = 'report.yudi_report_term_delivery.report_surat_jalan'

    @api.model
    def _get_report_values(self, docids, data=None):

        res = []
        invoices = []
        list_invoices = []
        list_dos = []

        picking_ids = self.env['stock.picking'].browse(docids)
        for pick in picking_ids:
            if pick.picking_type_id.code == 'outgoing' and pick.origin:
                picking_do = picking_ids.filtered(lambda pick: pick.picking_type_id.code == 'outgoing' and pick.origin)
                if picking_do:
                    if pick.deliv_type:
                        list_dos += pick.ids
                    else:
                        invoice_ids = pick.sale_id.invoice_ids
                        if invoice_ids:
                            list_invoices += invoice_ids.ids
                        else:
                          raise UserError(_("Source SO yang dipilih tidak meiliki invoice" \
                                +'\n' + "Silahkan Pilih data dengan Source document SO yang memiliki invoice"))
            else:
                raise UserError(_("Data pickings yang diperbolehkan adalah Delivery Order ke Customer dan Harus memiliki SO." \
                                +'\n' + "Silahkan Pilih data pickings Delivery Order ke Customer dan memiliki SO"))

        return {
            'doc_ids': docids,
            'doc_model': 'stock.picking',
            'docs': self.env['stock.picking'].browse(list_dos),
            'invoices': self.env['account.move'].browse(list_invoices),
            'report_type': data.get('report_type') if data else '',
        }
