# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models, _

class Mobil(models.Model):
    _name = 'mobil'
    _description = 'Mobil'

    name = fields.Char('No plat')
    vehicle_name = fields.Char()
    transmission = fields.Selection([('manual','Manual'),('automatic','Automatic')])
    fuel = fields.Selection([('pertamax','Pertamax'),('pertalite','Pertalite'),('solar','Solar')])
    color = fields.Char()
    mechine_number = fields.Char()
    chassis_number = fields.Char()
    max_load = fields.Float()
    image_1920 = fields.Image()
